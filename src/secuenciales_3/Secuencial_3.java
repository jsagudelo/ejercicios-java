
package secuenciales_3;

import java.util.Scanner;

public class Secuencial_3 {
    
    public static void main(String[] args) {
        
        
    double examen_mat, examen_qui, examen_fis, tareas_mat_1, tareas_mat_2, tareas_mat_3, 
           tareas_qui_1, tareas_qui_2, tareas_qui_3, tareas_fis_1, tareas_fis_2,promedio_tareas_mat,
           promedio_tareas_qui, promedio_tareas_fis, matematica, fisica, quimica;    
    
    
        Scanner lector = new Scanner(System.in);
        
        
        System.out.println(" ingrese el resultado de la tarea No 1 de matematicas : ");
        tareas_mat_1 = lector.nextInt();
        System.out.println(" ingrese el resultado de la tarea No 2 de matematicas : ");
        tareas_mat_2 = lector.nextInt();
        System.out.println(" ingrese el resultado de la tarea No 3 de matematicas : ");
        tareas_mat_3 = lector.nextInt();
        
        promedio_tareas_mat = (tareas_mat_1 + tareas_mat_2 + tareas_mat_3) / 3;
        
        System.out.println(" ingrese el resultado del examen de matematicas : ");
        examen_mat = lector.nextInt();
        
        matematica = (promedio_tareas_mat * 0.10) + (examen_mat * 0.90) ;
        
        System.out.println(" La calificacion de matematicas : " + matematica);
        
        
        System.out.println(" ingrese el resultado de la tarea No 1 de fisica : ");
        tareas_fis_1 = lector.nextInt();
        System.out.println(" ingrese el resultado de la tarea No 2 de fisica : ");
        tareas_fis_2 = lector.nextInt();
        
        promedio_tareas_fis = (tareas_fis_1 + tareas_fis_2) / 2;
        
        System.out.println(" ingrese el resultado del examen de fisica : ");
        examen_fis = lector.nextInt();
        
        fisica = promedio_tareas_fis * 0.20 + examen_fis * 0.80 ;
        
        System.out.println(" La calificacion de fisica : " + fisica);
        
        
        System.out.println(" ingrese el resultado de la tarea No 1 de quimica : ");
        tareas_qui_1 = lector.nextInt();
        System.out.println(" ingrese el resultado de la tarea No 2 de quimica : ");
        tareas_qui_2 = lector.nextInt();
        System.out.println(" ingrese el resultado de la tarea No 3 de quimica : ");
        tareas_qui_3 = lector.nextInt();
        
        promedio_tareas_qui = (tareas_qui_1 + tareas_qui_2 + tareas_qui_3) / 3;
        
        System.out.println(" ingrese el resultado del examen de quimica : ");
        examen_qui = lector.nextInt();
        
        quimica = promedio_tareas_qui * 0.15 + examen_qui * 0.85 ;
        
        System.out.println(" La calificacion de quimica : " + quimica);
        

        
    }
    
    
}
