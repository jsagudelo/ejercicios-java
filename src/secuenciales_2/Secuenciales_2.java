package secuenciales_2;

import java.util.Scanner;

public class Secuenciales_2 {


    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        int mazda = 19500000, toyota = 16000000, renault = 12000000, cantA, cantB, cantC, ventasTipoA, ventasTipoB, ventasTipoC,
                totalVentas ;
        double comisionA, comisionB, comisionC, comisionTotal;

        System.out.println("ingrese la cantidad de autos vendidos del tipo Mazda vendidos: ");
        cantA = lector.nextInt();
        System.out.println("ingrese la cantidad de autos vendidos del tipo Toyota vendidos: ");
        cantB = lector.nextInt();
        System.out.println("ingrese la cantidad de autos vendidos del tipo Renault vendidos: ");
        cantC = lector.nextInt();

        totalVentas = cantA * mazda + cantB * toyota + cantC * renault;
        ventasTipoA = cantA * mazda;
        ventasTipoB = cantB * toyota;
        ventasTipoC = cantC * renault;
        comisionA = (int) (ventasTipoA * 0.03);
        comisionB = (int) (ventasTipoB * 0.045);
        comisionC = (int) (ventasTipoC * 0.024);
        comisionTotal = comisionA + comisionB + comisionC;

        System.out.println("El valor de venta total es de " + totalVentas + " Total vendido");
        System.out.println("La comision de autos mazda " + comisionA + " Comision Mazda");
        System.out.println("La comision de autos toyota " + comisionB + " Comision Toyota");
        System.out.println("La comision de autos renault " + comisionC + " Comision Renault");
        System.out.println("La comision  total a pagar es de " + comisionTotal + " Total comision");
    }

}
    

