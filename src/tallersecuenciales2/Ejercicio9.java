
package tallersecuenciales2;
import java.util.Scanner;
public class Ejercicio9 {

    public static void main(String[] args) {
       double precioHora,horasLaboradas,sueldoBruto,descuentoRenta,salarioPago;
       Scanner objScanner = new Scanner(System.in);
       System.out.println("Ingrese la cantidad de horas laboradas: ");
       horasLaboradas=objScanner.nextDouble();
       System.out.println("Ingrese el precio de cada hora: ");
       precioHora=objScanner.nextDouble();
       sueldoBruto=horasLaboradas*precioHora;
       descuentoRenta=sueldoBruto*0.10;
       salarioPago=sueldoBruto-descuentoRenta;
       System.out.println("El sueldo bruto es: "+sueldoBruto);
       System.out.println("El descuento por impuesto de renta es: "+descuentoRenta);
       System.out.println("Salario a pagar: "+salarioPago);
       
      
    }
    
}
