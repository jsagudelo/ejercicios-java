
package tallersecuenciales2;
import java.util.Scanner;
public class Ejercicio11 {

    public static void main(String[] args) {
       double largoCampo,anchoCampo,metrosAlambre,pesoAlambre,rollos;
       Scanner objScanner = new Scanner(System.in);
       System.out.println("Ingrese el largor del campo en metros");
       largoCampo=objScanner.nextDouble();
       System.out.println("Ingrese el ancho del campo en metros");
       anchoCampo=objScanner.nextDouble();
       metrosAlambre=((largoCampo*2)+(anchoCampo*2))*5;
       pesoAlambre=metrosAlambre*1.45;
       rollos=metrosAlambre/200;
       System.out.println("La cantidad de alambre de puas necesario es: "+metrosAlambre+" Metros");
       System.out.println("El peso de "+metrosAlambre+" Metros de alambre es: "+pesoAlambre+" Kilogramos");
       System.out.println("La cantidad de rollos de alambre necesarios son: "+rollos);
    }
    
}
