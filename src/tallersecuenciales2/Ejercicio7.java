
package tallersecuenciales2;
import java.util.Scanner;
public class Ejercicio7 {

    public static void main(String[] args) {
       double notaExamenMat,notaExamenFi,notaExamenQui,tarea1Mat,tarea2Mat,tarea3Mat,
       tarea1Fi, tarea2Fi,tarea1Qui,tarea2Qui, promedioGenMat,promedioGenQui,promedioGenFi,
       notaFinal;
       Scanner objScanner = new Scanner(System.in);
       
       System.out.println("Ingrese la nota del examen de Matematicas: ");
       notaExamenMat=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 1 de Matematicas: ");
       tarea1Mat=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 2 de Matematicas: ");
       tarea2Mat=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 3 de Matematicas: ");
       tarea3Mat=objScanner.nextDouble();
       
       System.out.println("Ingrese la nota del examen de Fisica: ");
       notaExamenFi=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 1 de Fisica: ");
       tarea1Fi=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 2 de Fisica: ");
       tarea2Fi=objScanner.nextDouble();
       
       System.out.println("Ingrese la nota del examen de Quimica: ");
       notaExamenQui=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 1 de Quimica: ");
       tarea1Qui=objScanner.nextDouble();
       System.out.println("Ingrese la nota de la tarea 2 de Quimica: ");
       tarea2Qui=objScanner.nextDouble();
       
       promedioGenMat=( (notaExamenMat*90)+( ((tarea1Mat+tarea2Mat+tarea3Mat)/3)*10) )/100;
       promedioGenFi=( (notaExamenFi*80)+( ((tarea1Fi+tarea2Fi)/2)*20) )/100;
       promedioGenQui=( (notaExamenQui*85)+( ((tarea1Qui+tarea2Qui)/2)*15) )/100;
       notaFinal=(promedioGenMat+promedioGenFi+promedioGenQui)/3;
       
       System.out.println("Nota Final Matematicas: "+promedioGenMat);
       System.out.println("Nota Final Fisica: "+promedioGenFi);
       System.out.println("Nota Final Quimica: "+promedioGenQui);
       System.out.println("El promedio general es: "+notaFinal);
       
       
       
    }
    
}
