
package tallersecuenciales2;
import java.util.Scanner;
public class Ejercicio6 {

    public static void main(String[] args) {
       double precioProducto,precioCuota1,precioCuota2,precioCuota3;
       Scanner objScanner = new Scanner(System.in);
       System.out.println("Ingrese el valor del producto: ");
       precioProducto=objScanner.nextDouble();
       precioCuota1= precioProducto*0.5;
       precioCuota2=precioProducto*0.35;
       precioCuota3=precioProducto*0.15;
       System.out.println("Precio total del producto: "+precioProducto);
       System.out.println("El valor a pagar en la cuota 1 es: "+precioCuota1);
       System.out.println("El valor a pagar en la cuota 2 es: "+precioCuota2);
       System.out.println("El valor a pagar en la cuota 3 es: "+precioCuota3);
    }
    
}
