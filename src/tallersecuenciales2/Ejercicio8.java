
package tallersecuenciales2;
import java.util.Scanner;
public class Ejercicio8 {

    public static void main(String[] args) {
       double ventasSemanales,salario1, salario2, salario3, salario4, salario5, salarioTotal;
       Scanner objScanner = new Scanner(System.in);
       
       System.out.println("Ingrese el valor de las ventas semanales: ");
       ventasSemanales=objScanner.nextDouble();
       salario1=ventasSemanales*0.1;
       salario2=salario1*3;
       salario3=(salario1+salario2)/2;
       salario4=salario3/5;
       salario5=((salario3+salario1)/2)+(salario4*2);
       salarioTotal=salario1+salario2+salario3+salario4+salario5;
       
       System.out.println("El valor a pagar al empleado #1 es: "+salario1);
       System.out.println("El valor a pagar al empleado #2 es: "+salario2);
       System.out.println("El valor a pagar al empleado #3 es: "+salario3);
       System.out.println("El valor a pagar al empleado #4 es: "+salario4);
       System.out.println("El valor a pagar al empleado #5 es: "+salario5);
       System.out.println("El valor total a pagar por la empresa es: "+salarioTotal);
    }
    
}
