
package tallersecuenciales2;
import java.util.Scanner;
public class Ejercicio10 {

    public static void main(String[] args) {
       int horaIn,minIn,segIn;
       Scanner objScanner = new Scanner(System.in);
       System.out.println("Ingrese horas: ");
       horaIn=objScanner.nextInt();
       System.out.println("Ingrese minutos: ");
       minIn=objScanner.nextInt();
       System.out.println("Ingrese segundos: ");
       segIn=objScanner.nextInt();
       while(segIn>=60){
           segIn=segIn-60;
           minIn=minIn+1;
       }
       while(minIn>=60){
           minIn=minIn-60;
           horaIn=horaIn+1;
       }
       System.out.println("Tiempo Convertido: "+horaIn+":"+minIn+":"+segIn);
    }
    
}
