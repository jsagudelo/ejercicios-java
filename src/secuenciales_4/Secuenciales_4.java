
package secuenciales_4;

import java.util.Scanner;

public class Secuenciales_4 {

    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);

        double nota1, nota2, nota3, nota4, nota_final;

        System.out.println("ingrese la nota 1: ");
        nota1 = lector.nextDouble();

        System.out.println("ingrese la nota 2: ");
        nota2 = lector.nextDouble();

        System.out.println("ingrese la nota 3: ");
        nota3 = lector.nextDouble();

        System.out.println("ingrese la nota 4: ");
        nota4 = lector.nextDouble();

        nota_final = nota1 * 0.15 + nota2 * 0.20 + nota3 * 0.30 + nota4 * 0.35;

        System.out.println("la nota definitiva es: " + nota_final);
    }

}
